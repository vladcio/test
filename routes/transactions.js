var express = require('express');
var router = express.Router();
const Transaction = require('../models/transactions');


// get all transactions

router.get('/transactions', (req, res, next) => {
  Transaction.find({})
    .then((transactions) => res.json(transactions))
    .catch(next);
});

// get transaction by id

router.get('/transactions/:id', (req, res, next) => {
  Transaction.findById(req.params.id)
    .exec((err, transaction) => {
      if (err) { return res.json(err).status(500); }
      if (!transaction) { return res.json(err).status(404); }
      return res.json(transaction);
    });
});

// get shipment status

router.get('/transactions/:id/shipmentStatus', (req, res, next) => {
  Transaction.findById(req.params.id)
    .exec((err, transaction) => {
      if (err) { return res.json(err).status(500); }
      if (!transaction) { return res.json(err).status(404); }
      return res.json(transaction.shipment);
    });
});

// get payment status

router.get('/transactions/:id/paymentStatus', (req, res, next) => {
  Transaction.findById(req.params.id)
    .exec((err, transaction) => {
      if (err) { return res.json(err).status(500); }
      if (!transaction) { return res.json(err).status(404); }
      return res.json(transaction.payment);
    });
});

// add new transaction

router.post('/transactions', (req, res, next) => {
  const payment = {
    status: req.body.paymentStatus,
  };
  const shipment = {
    status: req.body.shipmentStatus,
  };

  const newTransaction = new Transaction({
    payment,
    shipment,
  });
    
  newTransaction.save((err) => {
    if (err) {
      let error = 'Something bad happened, try again!';
      res.status(400).json({ error: error });
      return;
    }
  });
 
  if (err) {
    let error = 'Something bad happened, try again!';
    res.status(500).json({ error: error });
  }
});

// handle add new payment/shipment

router.post('/transactions/:id', (req, res, next) => {
  const transactionId = req.params.id;
  if (!req.user) {
    res.redirect('/auth/login');
  }
  const paymentData = {
    status: req.body.paymentStatus,
  };

  const shipmentData = {
    status: req.body.paymentStatus,
  };

  Transaction.findByIdAndUpdate(transactionId, 
    { $push: { payment: paymentData } }, { $push: { shipment: shipmentData } })
    .then((transaction) => {
      return res.json(transaction);
      // navigate to transaction or else
     }).catch(err => {
      return next(err);
    });
});

module.exports = router;

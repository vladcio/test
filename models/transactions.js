'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// transaction schema
const transactionSchema = new Schema ({
  payment: [{
    status: String,
  }],
  shipment: [{
    status: String,
  }],
});

const Transaction = mongoose.model('Transaction', transactionSchema);

module.exports = Transaction;